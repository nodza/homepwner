//
//  BNRItem.h
//  Homepwner
//
//  Created by Noel Hwande on 7/16/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BNRItem : NSManagedObject

//@property (nonatomic, retain) NSString * itemName;
//@property (nonatomic, retain) NSString * serialNumber;
//@property (nonatomic) int32_t valueInDollars;
//@property (nonatomic) NSTimeInterval dateCreated;
//@property (nonatomic, retain) NSString * imageKey;
//@property (nonatomic, retain) NSData * thumbnailData;
//@property UNKNOWN_TYPE UNKNOWN_TYPE thumbnail;
//@property (nonatomic) double orderingValue;
//@property (nonatomic, retain) NSManagedObject *assetType;

@property (nonatomic, strong) NSString * itemName;
@property (nonatomic, strong) NSString * serialNumber;
@property (nonatomic) int32_t valueInDollars;
@property (nonatomic) NSTimeInterval dateCreated;
@property (nonatomic, strong) NSString * imageKey;
@property (nonatomic, strong) NSData * thumbnailData;
@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic) double orderingValue;
@property (nonatomic, strong) NSManagedObject *assetType;

- (void)setThumbnailDataFromImage:(UIImage *)image;

@end
