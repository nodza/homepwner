//
//  ImageViewController.h
//  Homepwner
//
//  Created by Noel Hwande on 7/15/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController
{
    __weak IBOutlet UIImageView *imageView;
    __weak IBOutlet UIScrollView *scrollView;
}

@property (nonatomic, strong) UIImage *image;

@end
