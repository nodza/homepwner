//
//  AssetTypePicker.h
//  Homepwner
//
//  Created by Noel Hwande on 7/17/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BNRItem;

@interface AssetTypePicker : UITableViewController

@property (nonatomic, strong) BNRItem *item;

@end
