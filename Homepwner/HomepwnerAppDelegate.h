//
//  HomepwnerAppDelegate.h
//  Homepwner
//
//  Created by Noel Hwande on 7/5/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomepwnerAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
