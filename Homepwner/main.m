//
//  main.m
//  Homepwner
//
//  Created by Noel Hwande on 7/5/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HomepwnerAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HomepwnerAppDelegate class]));
    }
}
