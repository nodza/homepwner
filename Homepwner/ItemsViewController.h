//
//  itemsViewController.h
//  Homepwner
//
//  Created by Noel Hwande on 7/5/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HomepwnerItemCell.h"

@interface ItemsViewController : UITableViewController<UIPopoverControllerDelegate>
{
    UIPopoverController *imagePopover;
}

- (IBAction)addNewItem:(id)sender;

@end
