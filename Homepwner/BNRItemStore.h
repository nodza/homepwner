//
//  BNRItemStore.h
//  Homepwner
//
//  Created by Noel Hwande on 7/5/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BNRItem;

@interface BNRItemStore : NSObject
{
    NSMutableArray *allItems;
    NSMutableArray *allAssetTypes;
    NSManagedObjectContext *context;
    NSManagedObjectModel *model;
}

// This is a class method so it is prefixed with a + instead of a -
+ (BNRItemStore *)sharedStore;

- (void)removeItem:(BNRItem *)p;
- (void)moveItemAtIndex:(int)from toIndex:(int)to;

- (NSArray *)allItems;
- (BNRItem *)createItem;
- (NSString *)itemArchivePath;
- (BOOL)saveChanges;

- (NSArray *)allAssetTypes;

@end
