//
//  ImageViewController.m
//  Homepwner
//
//  Created by Noel Hwande on 7/15/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import "ImageViewController.h"

@implementation ImageViewController
@synthesize image;

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGSize sz = [[self image] size];
    [scrollView setContentSize:sz];
    
    [imageView setFrame:CGRectMake(0, 0, sz.width, sz.height)];
    
    [imageView setImage:[self image]];
}

@end
